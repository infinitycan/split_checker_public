from re import compile
from checker_util import eprint
from argparse import ArgumentParser, REMAINDER
from checker_thread import Checker
from checker_util import ILP, EXHAUSTIVE
from checker_input import Generator_Input, File_Input, K_Elem_Random_Input, NODE, NUM_NODES
from checker_output import One_Output, Many_Output, Print_Output, No_Output
from multiprocessing import Queue

FILE_INPUT, GENERATOR_INPUT, RANDOM_K_INPUT = 0, 1,2
PRINT_OUTPUT, MANY_OUTPUT, ONE_OUTPUT, NO_OUTPUT = 'print','many','one','none'
def input_type(specifier):
    """
    Eats an input specifier and spits out what input type to make
    """
    if len(specifier) == 1:
        specifier_string = specifier[0].replace(' ','')

        # check if generator specifier
        generator_specifier_pattern = compile('[0-9]+;[0-9](,[0-9])*;[0-9]+/[0-9]+')
        m = generator_specifier_pattern.match(specifier_string)
        if m != None and m.group() == specifier_string:
            return GENERATOR_INPUT

        # otherwise check if random_k specifier
        random_k_specifier_patter = compile('[0-9]+;[0-9]+;[0-9]+')
        m = random_k_specifier_patter.match(specifier_string)
        if m != None and m.group() == specifier_string:
            return RANDOM_K_INPUT
    return FILE_INPUT

def parse_generator_specifier(specifier_string):
    """
    parse a generator specifier
    """
    specifier_string_segments = specifier_string.split(';')
    num_sets = int(specifier_string_segments[0])
    number_list = [ int(i) for i in specifier_string_segments[1].split(',') ]
    node = int(specifier_string_segments[2].split('/')[NODE])
    num_nodes = int(specifier_string_segments[2].split('/')[NUM_NODES])
    if num_nodes < 1:
        eprint("ERROR: number of nodes must be positive")
    if node < 1 or node > num_nodes:
        eprint("ERROR: node number must be an integer in range [1, number_of_nodes]")
    return [num_sets, number_list, (node, num_nodes)]

def parse_random_k_specifier(specifier_string):
    """
    parse a random k element input speciier
    """
    specifier_string_segments = specifier_string.split(';')
    num_sets = int(specifier_string_segments[0])
    num_elems = int(specifier_string_segments[1])
    num_cases = int(specifier_string_segments[2])
    return [num_sets, num_elems, num_cases]

# parse arguments
parser = ArgumentParser()
parser.add_argument('-i','--ilp', help='run full ilp solve',action='store_true')
parser.add_argument('-e','--exhaustive', help='run exhaustive search (not implemented)',action='store_true')
parser.add_argument('-d','--decimal', help='use decimal indexing for input file regions',action='store_true')
parser.add_argument('-o','--output', help='output method:\n\tone: print all results to one file\n\tmany (default): print each input file to its own output file\n\tprint: print to stdout\n\tnone: no output',action='store', default="many", choices=['one', 'many', 'print', 'none'])
parser.add_argument('-t','--threads', help='number of threads; 0 for no maximum (default = 4)',action='store', default=4)
parser.add_argument('-s','--default_size', help='default size for unspecified regions',action='store', default=0)
parser.add_argument('--output_all', help='output all cases checked, not just the unsplittable ones', action='store_true')
parser.add_argument('input_specifier', help='input file name(s) or specify generation parameters',action='store',nargs=REMAINDER)
parser.add_argument('-q','--queue_size', help='size of i/o queues that compute threads pull and push to and from (default = 1000, 0 = unlimited)', default=1000)
#parser.add_argument('-O','--output_dir', help='directory for output (default = .)', default='.')
args = parser.parse_args()

num_threads = int(args.threads)
max_queue_size = int(args.queue_size)

# set up input thread
input_q = Queue(max_queue_size)
input_type = input_type(args.input_specifier) 
base = 10 if args.decimal else 2
if input_type == GENERATOR_INPUT:
    generator_specifier = parse_generator_specifier(args.input_specifier[0])
    input_source = Generator_Input(num_sets = generator_specifier[0],
                                   numbers = generator_specifier[1],
                                   node_specifier = generator_specifier[2],
                                   queue = input_q,
                                   num_to_kill = num_threads)
elif input_type == FILE_INPUT:
    input_source = File_Input( file_names = args.input_specifier, 
                                index_base = base,
                                queue = input_q,
                                num_to_kill = num_threads,
                                default_size = int(args.default_size))
elif input_type == RANDOM_K_INPUT:
    random_k_specifier = parse_random_k_specifier(args.input_specifier[0])
    input_source = K_Elem_Random_Input(num_sets = random_k_specifier[0],
                                   num_elems = random_k_specifier[1],
                                   num_cases = random_k_specifier[2],
                                   queue = input_q,
                                   num_to_kill = num_threads)
else:
    eprint("ERROR: Input type not recognized")
    exit()

input_source.start()

# set up output thread
output_q = Queue(max_queue_size)
if args.output == PRINT_OUTPUT:
    output_source = Print_Output(queue = output_q, 
                                num_to_die = num_threads,
                                output_all = args.output_all)
elif args.output == MANY_OUTPUT:
    output_source = Many_Output(queue = output_q, 
                                num_to_die = num_threads,
                                output_all = args.output_all)
elif args.output == ONE_OUTPUT:
    output_source = One_Output(queue = output_q, 
                               num_to_die = num_threads,
                               output_all = args.output_all)
elif args.output == NO_OUTPUT:
    output_source = No_Output(queue = output_q, 
                               num_to_die = num_threads,
                               output_all = args.output_all)
else:
    eprint("ERROR: Output type not recognized")
    exit()

output_source.start()

# set up computation threads
options = dict()
options[ILP] = args.ilp
options[EXHAUSTIVE] = args.exhaustive
for i in range(num_threads):
    c = Checker(input_q, output_q, options)
    c.start()
