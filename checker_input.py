from multiprocessing import Process
from checker_util import eprint, NO_MORE_TO_DO, SIZE, OUTPUT_DECORATION
from math import log, ceil
from random import randint

NODE = 0
NUM_NODES = 1

class Abstract_Input(Process):
    """
    Abstract base class for an input provider
    """
    def __init__(self, queue, num_to_kill, index_base, default_size):
        super(Abstract_Input, self).__init__()
        self.q = queue
        self.num_to_kill = num_to_kill # number of compute procs to kill
        self.base = index_base
        self.default_size = default_size

    def run(self):
        self.act()

        # signal computation threads that no more input is coming
        for i in xrange(self.num_to_kill):
            self.q.put(NO_MORE_TO_DO)

    def act(self):
        pass

class Abstract_File_Input(Abstract_Input):
    """
    Abstract base class for an input that reads files in some manner and puts their contents on the queue
    """
    def __init__(self, queue, file_names, num_to_kill, index_base=2, default_size = 0):
        super(Abstract_File_Input, self).__init__(queue, num_to_kill, index_base, default_size)
        self.file_names = file_names

    def act(self):
        while len(self.file_names) > 0:
            name = self.file_names.pop()
            f = None
            try:
                f = open(name)
                file_contents = self.parse_file_contents(f)
                self.q.put(file_contents + [name])
            except IOError:
                eprint("ERROR: Could not read file \"" + name + "\"")
            finally:
                if f != None:
                    f.close()

    def parse_file_contents(self, open_file):
        """
        Return: [number of sets, region sizes]
        """
        pass

class File_Input(Abstract_File_Input):
    def __init__(self, queue, file_names, num_to_kill, index_base=2, default_size = 0):
        super(File_Input, self).__init__(queue, file_names, num_to_kill, index_base, default_size)

    def parse_file_contents(self, open_file):
        """
        Method for processing an indexed serial file
        open_file has format:
            number of sets (=n)
            region index, region size
            .
            .
            .
            region index, region size
    
    
        The region index is assumed to be a binary string unless 
            decimal_indexing=True
        is passed.
    
        Any unspecified regions are given size k when
            default_size = k
        is passed, and zero otherwise.
    
        Return: [number of sets, region sizes]
        """
        num_sets = int(open_file.readline())
        size = [self.default_size for i in xrange(2**num_sets)]
    
        INDEX = 0
        VALUE = 1
        for line in open_file:
            parts = line.split(',')
            size[int(parts[INDEX], self.base)] = int(parts[VALUE])
        return [num_sets, size]

class Own_Output_Input(Abstract_File_Input):
    def __init__(self, queue, file_names, num_to_kill, index_base=2, default_size = 0):
        super(Own_Output_Input, self).__init__(queue, file_names, num_to_kill, index_base, default_size)

    def parse_file_contents(self, open_file):
        """
        reads in files in the output format of split_checker
        Return: [number of sets, region sizes]
        """
        line = ""
        while line != OUTPUT_DECORATION + SIZE  + OUTPUT_DECORATION:
            line = open_file.readline()
        line = open_file.readline()
        size = parse_list(line)
        num_sets = int(log( len(nums), 2))
        return [num_sets, nums]

    def parse_list(list_string):
        """
        given a string of the form
         '[n1,n2,n3,....,nN]'
         returns a list of the integers n1, n2, ..., nN
         """
        return [ int(i) for i in list_string[1:len(list_string)-2].split(',') ]


class Generator_Input(Abstract_Input):
    def __init__(self, num_sets, numbers, queue, num_to_kill, node_specifier, index_base=2, default_size = 0):
        super(Generator_Input, self).__init__(queue, num_to_kill, index_base, default_size)
        self.num_sets = num_sets
        self.num_regions = 2**self.num_sets
        self.numbers = numbers
        self.bits_per_region = int(ceil(log( len(self.numbers), 2 )))
        self.node_specifier = node_specifier

    def act(self):
        mask = sum( [ 2**k for k in xrange(self.bits_per_region) ] )
        number_of_cases = 2**(self.bits_per_region * (self.num_regions - 1))
        first_case = (self.node_specifier[NODE]-1) * number_of_cases / self.node_specifier[NUM_NODES] # make sure integer division is done here
        last_case = min(number_of_cases, self.node_specifier[NODE] * number_of_cases / self.node_specifier[NUM_NODES])

        for case in xrange(first_case, last_case):
            casedata = case << self.bits_per_region

            # check that only the number we want are there
            casedata_is_good = True 

            size = []               # holds regions caps
            for r in xrange(self.num_regions):
                number_index = (casedata >> self.bits_per_region*r) & mask
                if not number_index < len(self.numbers):
                    casedata_is_good = False
                    break
                else:
                    size += [self.numbers[number_index]]

            if casedata_is_good:
                size[0] = 0
                self.q.put([self.num_sets, size, casedata])


class K_Elem_Random_Input(Abstract_Input):
    def __init__(self, num_sets, num_elems, num_cases, queue, num_to_kill, index_base=2, default_size = 0):
        super(K_Elem_Random_Input, self).__init__(queue, num_to_kill, index_base, default_size)
        self.num_sets = num_sets
        self.num_regions = 2**self.num_sets
        self.num_elems= num_elems
        self.num_cases = num_cases
        self.casedata_cache = set()

    def act(self):
        counter = 0
        while counter < self.num_cases:
            size = [0 for i in xrange(self.num_regions)]
            for i in range(self.num_elems):
                size[randint(1, self.num_regions-1)] += 1
            casedata = 0
            for i in range(1, len(size)):
                casedata += 10**i * size[i]
            if casedata in self.casedata_cache:
                continue
            else:
                self.casedata_cache.add(casedata)
                self.q.put([self.num_sets, size, casedata])
                counter += 1
