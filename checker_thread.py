from multiprocessing import Process, Lock
from sage.all import *
from math import floor,ceil
from sage.numerical.mip import MIPSolverException
from checker_util import SPLITTABLE, ILP, EXHAUSTIVE, NO_MORE_TO_DO, SIZE, NUM_SETS 

MULTIPLICITIES = dict()
MULTIPLICITIES_LOCK = Lock()
REGIONS_OF_SETS = dict()
REGIONS_OF_SETS_LOCK = Lock()
FAIL_MSG = None

class Checker(Process):
    def __init__(self, input_q, output_q, options):
        super(Checker, self).__init__()
        self.options = options
        self.input_q = input_q 
        self.output_q = output_q
        self.num_sets =  self.size = self.num_regions = self.regions_of_sets = self.multiplicity = None
        self.case_metadata = None

    def fetch_case(self):
        
        old_num_sets = self.num_sets
        q_item = self.input_q.get()

        if q_item == NO_MORE_TO_DO:
            self.output_q.put(NO_MORE_TO_DO)
            return False
        self.num_sets, self.size, self.case_metadata = q_item
        if self.num_sets != old_num_sets:
            self.num_regions = 2**self.num_sets
            self.regions_of_sets = calc_regions_of_sets(self.num_sets)
            self.multiplicity = calc_multiplicities(self.num_regions)
        return True

    def run(self):
        while self.fetch_case():
            algebra_ans = None
            ilp_ans = FAIL_MSG
            results = dict()
    
            if self.options[ILP]:
                ilp_ans = self.ilp_solve()
                results[ILP] = ilp_ans
            results[SPLITTABLE] = (ilp_ans != FAIL_MSG)
            results[SIZE] = self.size
            results[NUM_SETS] = self.num_sets

            self.output_q.put([results, self.case_metadata])
    
    def ilp_solve(self):
        """
        Builds a Sage ILP solver to see if there are any actual solutions
        """
        p = MixedIntegerLinearProgram()
        x = p.new_variable(integer=True, nonnegative=True)
        p.set_objective(None)

        var_dict = dict()
        for i in xrange(self.num_regions):
            p.add_constraint( x[i] <= self.size[i] )
            x_str = "x_" + str(i)
            var_dict[x_str] = x[i]

        set_sizes = [ 0 for i in range(self.num_sets) ]
        for s in range(self.num_sets):
            set_size = 0
            set_expr_string = ""
            for r in self.regions_of_sets[s]:
                set_size += self.size[r]
                set_expr_string += "x_" + str(r) + "+"
            set_expr_string = (set_expr_string)[ :len(set_expr_string) - 1]
            set_expr = sage_eval(set_expr_string, locals=var_dict)
            p.add_constraint( set_expr, min=floor(set_size / 2.0), max = ceil(set_size / 2.0) )

        try:
            p.solve()
        except MIPSolverException as e:
            return FAIL_MSG

        return p.get_values(x)

def calc_regions_of_sets(num_sets):
    """
    num_sets = number of sets
    generates a list indexed by the indices of the sets
    the ith space contains a list of region indices that are subsets of set i
    """
    with REGIONS_OF_SETS_LOCK:
        if not num_sets in REGIONS_OF_SETS:
            regions_of_sets = [ [] for i in range(num_sets) ]
            for r in xrange(2**num_sets):
                for s in range(num_sets):
                    mask = 1 << s
                    if r & mask != 0 :
                        regions_of_sets[s] += [r]
            REGIONS_OF_SETS[num_sets] = regions_of_sets
        return REGIONS_OF_SETS[num_sets]

def calc_multiplicities(num_regions):
    """
    n = number of sets
    return a list whose ith index is the multiplicity of region i 
        (how many sets region i is in)
    """
    with MULTIPLICITIES_LOCK:
        if not num_regions in MULTIPLICITIES:
            mults = []
            for i in xrange(num_regions):
                elems = 0
                while(i > 0):
                    elems += i & 1
                    i = i >> 1
                mults += [elems]
            MULTIPLICITIES[num_regions] = mults
        return MULTIPLICITIES[num_regions]
