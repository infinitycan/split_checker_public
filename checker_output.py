from multiprocessing import Process, Queue
from checker_util import eprint
from checker_util import SPLITTABLE, ILP, EXHAUSTIVE, SIZE, NUM_SETS, NO_MORE_TO_DO, OUTPUT_DECORATION

SEPARATOR = '--------------------------------------\n'

class Output(Process):
    def __init__(self, queue, num_to_die, output_all):
        super(Output, self).__init__()
        self.q = queue
        self.num_to_die = num_to_die
        self.dead_procs = 0
        self.result = None
        self.case_metadata = None
        self.output_all = output_all

    def run(self):
        while self.dead_procs < self.num_to_die:
            q_item = self.q.get()
            if q_item == NO_MORE_TO_DO:
                self.dead_procs += 1
                continue
            else:
                self.result, self.case_metadata = q_item
                if self.result[SPLITTABLE] and not self.output_all:
                    continue
                else:
                    self.act()

    def act(self):
        pass

class Print_Output(Output):
    """
    Output to stdout
    """
    def __init__(self,queue, num_to_die, output_all = False):
        super(Print_Output, self).__init__(queue, num_to_die, output_all)

    def act(self):
        print(build_output_string( self.result, label= ("" if self.case_metadata == None else self.case_metadata) ))
        print(SEPARATOR)

class Many_Output(Output):
    """
    Output to many files
    """
    def __init__(self,queue, num_to_die, output_all = False):
        super(Many_Output, self).__init__(queue, num_to_die, output_all)
        self.counter = 0

    def act(self):
        output_string = build_output_string(self.result)
        f = None
        try:
            file_name = (str(self.case_metadata) + ".output") if self.case_metadata != None else (str(self.counter) + ".output")
            self.counter += 1
            f = open(file_name, 'w')
            f.write(output_string)
        except IOError:
            eprint("ERROR: Could not write to file \"" + name + "\"")
        finally:
            if f != None:
                f.close()

class One_Output(Output):
    """
    Output to a single file
    """
    def __init__(self, queue, num_to_die, output_all = False):
        super(One_Output, self).__init__(queue, num_to_die, output_all)
        self.output_file = None
        self.have_deleted_old_output = False

    def act(self):
        # if we don't know where to put the file, try to extract it from the input path
        if self.output_file == None:
            if self.case_metadata != None:
                output_dir = ""
                if type(self.case_metadata) is str:
                    i = len(self.case_metadata)-1
                    while self.case_metadata[i] != '/' and i >= 0:
                        i -= 1
                    output_dir = './' if i < 0 else self.case_metadata[:i]
                elif type(self.case_metadata) is int:
                    output_dir = './'
                else:
                    eprint('ERROR: unrecognized case_metadata type')
                self.output_file = output_dir + "/checker_output.output"
            else:
                self.output_file = './checker_output.output'
        f = None
        try:
            if not self.have_deleted_old_output:
                f = open(self.output_file, 'w')
                self.have_deleted_old_output = True
            else:
                f = open(self.output_file, 'a')
            f.write(build_output_string(self.result, label= "" if self.case_metadata == None else self.case_metadata))
            f.write(SEPARATOR)
        except IOError:
            eprint("ERROR: Could not write to file \"" + name + "\"")
        finally:
            if f != None:
                f.close()

class No_Output(Output):
    """
    Output nothing
    """
    def __init__(self,queue, num_to_die, output_all = False):
        super(No_Output, self).__init__(queue, num_to_die, output_all)

    def act(self):
        pass


def build_output_string(results, label=""):
    output_string = ""
    if label != "":
        output_string +=  str(label) + "\n" 
    for k in results.keys():
        output_string += OUTPUT_DECORATION + str(k) + OUTPUT_DECORATION + "\n"
        output_string += str(results[k]) + '\n'
    return output_string
