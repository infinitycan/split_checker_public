from __future__ import print_function
import sys

SPLITTABLE, ILP, EXHAUSTIVE = "splittable","ilp","exhaustive", 
SIZE, NUM_SETS = "size","num_sets"
NO_MORE_TO_DO = None
OUTPUT_DECORATION = "--"

def eprint(*args, **kwargs):
    """
    print to stderr
    """
    print(*args, file=sys.stderr, **kwargs)
